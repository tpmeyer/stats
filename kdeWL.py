#########################################
#
#
#########################################

import numpy as np
import matplotlib.pyplot as plt

class kdeWL(object):

    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks = None,min_cnt=10):
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts)/(ax1_max-ax1_min)
       self.W =  np.zeros(ax1_pts)
       self.L = np.zeros(ax1_pts)

    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
   
    def p_of_i(self,i):
        p =  (1.0 + self.W[i]) / (2.0 + self.W[i]+ self.L[i])
        #print ("DING",i, self.W[i],self.L[i],p)
        return (1.0 + self.W[i]) / (2.0 + self.W[i]+ self.L[i])

    def prob_dict(self,pdict):
        rv = dict() 
        for n,v in pdict.items():
            idx = self.ax1_to_i(n)
            rv [ n ] = self.p_of_i(idx)
        return rv

    def dump(self,all=True):
        xplt = []
        yplt = []
        for i in range(self.ax1_pts):
            if all or self.W[i] + self.L[i] > 0.01:
               print ("@ %.2f :\t[ %.2f - %.2f ]\tp(W)=%.2f" %(self.i_to_ax1(i),self.W[i],self.L[i],self.p_of_i(i) ) )
               xplt.append(self.i_to_ax1(i))
               yplt.append( self.p_of_i(i) )
        plt.plot(xplt,yplt)
        plt.show()

    #at X drop Y
    def drop(self, ax1,valy, ks = None):#triangle update
        if ks == None:
           ks = self.ax1-ks
        if ks == None:
           print("Need Kernel Size")
           quit()
        imin = max(1+self.ax1_to_i(ax1-ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+ks/2.0),self.ax1_pts-1)
        hgt = 1.0
        #print (imin,imax)
        for i in range(imin,imax+1):
            atten =  hgt*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /ks )
            if atten  < 0.0:
                print ("CRAP",atten)
                quit()
            #print ("DOING",i, ax1,self.i_to_ax1(i),atten)
            #print ("\t\t",i, ax1, self., ks = Non, ks = Noneei_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks)
            if valy > 0.0:
               self.W[i] += atten*valy;
            else:
               self.L[i] -= atten*valy;
        #quit()
if __name__ == '__main__':

    ax1_min = -30
    ax1_max = 30
    ax1_pts = 10*(ax1_max - ax1_min)
    g = kdeWL( ax1_min,ax1_max,ax1_pts)
    ks = abs(3.0)  * 0.7 + 2
    g.drop(10,1,ks)
    g.drop(10.1,1,ks)
    g.drop(10.3,1,ks)
    g.drop(10.5,1,ks)
    g.drop(10.22,1,ks)
    g.drop(9.4,-1,ks)
    g.drop(9,-1,ks)
    g.drop(9.1,-1,ks)
    print("g.dump(False)")
    g.dump(False)
