import numpy as np

class ema2d(object):

    def __init__(self, decay):
        self.nd = 1.0 - decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
        self.ww = 0.0
        self.N = 0

    def update(self, valx, valy):
        self.x = self.nd*self.x + valx;
        self.xx = self.nd*self.xx + valx*valx;
        self.y = self.nd*self.y + valy;
        self.yy = self.nd*self.yy + valy*valy;
        self.xy = self.nd*self.xy + valx*valy;
        self.w = self.nd*self.w + 1.0;
        self.ww = self.nd*self.ww + 1.0;
        self.N += 1

    def corr(self):
        return (self.w*self.xy - self.x*self.y) / np.sqrt( (self.w*self.xx - self.x**2) * (self.w*self.yy - self.y**2) ) 

    #for beta, market has to be y vector
    def beta(self): return self.vxy() / self.vy()
    def vx(self): return (self.xx*self.w - self.x*self.x) / (self.w*self.w-self.ww)
    def vy(self): return (self.yy*self.w - self.y*self.y) / (self.w*self.w-self.ww)
    def vxy(self): return (self.xy*self.w - self.x*self.y) / (self.w*self.w-self.ww)
    def xval(self): return self.x
    def xxval(self): return self.xx

class ema(object):

    def __init__(self, decay):
        self.nd = 1.0 - decay
        self.x = 0.0
        self.w = 0.0

    def update(self, x):
        self.x = self.nd*self.x + x
        self.w = self.nd*self.w +1.0
        return self.x / self.w

    def last(self):
        if self.w == 0: return 0.0
        return self.x / self.w

class rma(object):
    def __init__(self, decay):
        self.d = decay
        self.bday = -99.99
        self.age = 0
        self.x = 0.0
        self.w = 0.0

    def update(self, x,t):
        if self.bday < 0.0:
            self.bday = t
            self.age = 0
            self.x = x;
            self.w = 1;
            return None
        t = t - self.bday
        #print x,t,self.w,self.age,self.x
        self.w /= math.exp(self.d*(t - self.age) );
        #print x,t,self.w,self.age,self.x
        self.x = (self.w*self.x + x)/(self.w)
        self.age = t;
        return self.x / self.age;

    def last(self,t):
        self.age = t - self.bday;
        return self.x / self.age

class sprma(object):
    def __init__(self, halflife):
        self.nd = math.exp(-math.log(2.0)/halflife)
        self.xsum = 0.0
        self.tsum = 0.0
        self.t_prev = -990.0

    def update(self, x,t):
        if self.t_prev < 0.0:
            self.t_prev = t
            self.xsum = x;
            return None
        dt = t - self.t_prev
        self.t_prev = t
        if dt == 0:
            self.xsum = self.xsum + x
        else:
            for iter in range(1,dt):
                self.xsum = self.nd*self.xsum 
                self.tsum = self.nd*self.tsum + 1
            self.xsum = self.nd*self.xsum + x
            self.tsum = self.nd*self.tsum + 1
        return self.xsum / self.tsum

    def last(self,t):
        dt = t - self.t_prev
        self.t_prev = t
        for iter in range(0,dt):
            self.xsum = self.nd*self.xsum 
            self.tsum = self.nd*self.tsum + dt
        return self.xsum / self.tsum


class tma2d(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
    def update(self,x,y,t):
       if (self.w < 0.0001): #first time through...
           self.age = t
           self.x= x
           self.y= y
           self.xx= x*x
           self.yy= y*y
           self.xy= x*y
           self.w = 1.0
           return
       else:
           
           z = (self.age-t) * self.decay  #negative number
           # taylor approximation less than 10^-4 error for abs(z) < 0.10
           if z > -0.01:
               #print ("approx this",np.exp(z),"with this",1+z+z*z/2.0)
               self.w *= (1.0 + z + z*z/2.0)
           else: self.w *= np.exp(z)

           self.x = (x + self.x * self.w) / (1.0+self.w)
           self.y = (y + self.y * self.w) / (1.0+self.w)
           self.xx = (x*x + self.xx * self.w) / (1.0+self.w)
           self.xy = (x*y + self.xy * self.w) / (1.0+self.w)
           self.yy = (y*y + self.yy * self.w) / (1.0+self.w)
           self.age = t
           self.w += 1.0

    def vx(self): return (self.xx - self.x*self.x)*self.w/(self.w-1.0)
    def vy(self): return (self.yy - self.y*self.y)*self.w/(self.w-1.0)
    def vxy(self): return (self.xy - self.x*self.y)*self.w/(self.w-1.0)

class tma(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.w = 0.0

    def update(self, x,t):
        if (self.w < 0.0001):
           self.age = t
           self.x= x
           self.w = 1.0
           return x
        else:
           up = (self.age-t) * self.decay  #negative number
           #print (up / self.decay)
           if up > -0.1:   # 10^-4 error
              self.w *= (1.0 + up + up*up/2.0)
           else:
              self.w *= np.exp(up)
        self.x = (x + self.x * self.w) / (1+self.w)
        self.age = t
        self.w += 1.0
        return self.x

    def last(self):
        return self.x

class tmaStats(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
    def update(self,x,t):
       if (self.w < 0.0001): #first time through...
           self.age = t
           self.x= x
           self.xx= x*x
           self.w = 1.0
           return
       else:
           
           z = (self.age-t) * self.decay  #negative number
           # taylor approximation less than 10^-4 error for abs(z) < 0.10
           if z > -0.01:
               #print ("approx this",np.exp(z),"with this",1+z+z*z/2.0)
               self.w *= (1.0 + z + z*z/2.0)
           else: self.w *= np.exp(z)

           self.x = (x + self.x * self.w) / (1.0+self.w)
           self.xx = (x*x + self.xx * self.w) / (1.0+self.w)
           self.age = t
           self.w += 1.0
           return

    def x(self): return self.x/self.w
    def std(self): return np.sqrt(self.var() )
    def var(self): return (self.xx - self.x*self.x)*self.w/(self.w-1.0)

