#########################################
#
# SPACE is the X direction
# for a given X, there is a value Z
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# along the X direction
#
#########################################

import numpy as np

class kde1D(object):

    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,min_cnt=10):
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts-1)/(ax1_max-ax1_min)
       self.x =  np.zeros(ax1_pts) 
       self.xx = np.zeros(ax1_pts)
       self.y =  np.zeros(ax1_pts)
       self.yy = np.zeros(ax1_pts)
       self.xy = np.zeros(ax1_pts)
       self.w =  np.zeros(ax1_pts)
       self.absw = np.zeros(ax1_pts)

    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
   
    def get_w(self):
        return self.w
    def grid_xy(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.xy[i] )
        return x,y

    def grid_cnt(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.w[i] )
        return x,y

    def grid_roi(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            if self.absw[i] > self.min_cnt:
               y.append ( self.xy[i]  / (self.min_cnt+self.absw[i]) )
            else:
               y.append(np.float64('nan'))
        return x,y

    def grid_corr(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            if self.w[i] > self.min_cnt and self.w[i]*self.xx[i] > self.x[i]*self.x[i] and self.w[i]*self.yy[i] > self.y[i]*self.y[i]:
               y.append ( (self.w[i]*self.xy[i] - self.x[i]*self.y[i]) /
                 np.sqrt( (self.w[i]*self.xx[i] - self.x[i]**2) * (self.w[i]*self.yy[i] - self.y[i]**2) ) )
            else:
               y.append(np.float64('nan'))
        return x,y

    def corr(self,val_ax1):
        i = self.ax1_to_i(val_ax1)
        return ( (self.w[i]*self.xy[i] - self.x[i]*self.y[i]) /
                 np.sqrt( (self.w[i]*self.xx[i] - self.x[i]**2) * (self.w[i]*self.yy[i] - self.y[i]**2) ) )

    def update(self, ax1,valx,valy):#triangle update
        imin = max(1+self.ax1_to_i(ax1-self.ax1_ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+self.ax1_ks/2.0),self.ax1_pts-1)
        if imin == imax:
            hgt = 1.0
            #print ("ONLY ONE POINT")
            imax += 1
        else:
            hgt = 2.0 / (imax-imin) #weight at peak
        #print (imin,imax)
        asum = 0.0
        for i in range(imin,imax):
            atten =  hgt*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks )
            if atten  > 0.0: #else OUTSIDE OF GRID!
            #atten = 1.0
            #print (i, ax1,self.i_to_ax1(i),atten)
               asum += atten
               self.x[i] += atten*valx;
               self.y[i] += atten*valy;
               self.xx[i] += atten*valx*valx;
               self.yy[i] += atten*valy*valy;
               self.xy[i] += atten*valx*valy;
               self.w[i] += atten
               self.absw[i] += abs(atten)
        return



#########################################
#
# SPACE is the (X,Y) plane
# for a given (X,Y), there is a value Z
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# for every point on the (X,Y) plane
#
#########################################

class kde2D(object):


    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,
                       ax2_min,ax2_max,ax2_pts,ax2_ks,
            min_cnt=10):
       xspc = (ax1_max-ax1_min+1)/ax1_pts
       yspc = (ax2_max-ax2_min+1)/ax2_pts
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts-1)/(ax1_max-ax1_min)
       self.ax2_min = ax2_min
       self.ax2_max = ax2_max
       self.ax2_pts = ax2_pts
       self.ax2_ks = ax2_ks
       self.ax2_scale = float(ax2_pts-1)/(ax2_max-ax2_min)
       self.x =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.xx =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.y =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.yy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.xy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.xz =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.yz =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.zz =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.w =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.absw = np.zeros(shape=(ax1_pts,ax2_pts))
       #print (self.ax1_scale)
       


    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def ax2_to_j(self, ax2): return int(self.ax2_scale*(ax2-self.ax2_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
    def j_to_ax2(self, j): return float(j)/self.ax2_scale+self.ax2_min
   
    def get_xyw(self):
        return self.xgrid,self.ygrid,self.w
    def get_w(self):
        return self.w
    def grid_cnt(self):
        xyz = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xyz.append( ( (self.i_to_ax1(i), self.j_to_ax2(j) ) , self.w[i][j] ) )
        return xyz

    def grid_roi(self,axis=0):
        rv =  np.zeros(shape=(self.ax1_pts,self.ax2_pts))
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               if self.absw[i][j] > self.min_cnt:
                   if axis == 0:
                      rv[i][j] =  self.xz[i][j]  / (self.min_cnt+self.absw[i][j])
                   else:
                      rv[i][j] =  self.yz[i][j]  / (self.min_cnt+self.absw[i][j])
               else:
                   rv[i][j] = 0.0
        return rv

    def list_roi(self,axis=0):
        xy = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xy.append( (self.i_to_ax1(i), self.j_to_ax2(j) ) )
               if self.absw[i][j] > self.min_cnt:
                   if axis == 0:
                      z.append ( self.xz[i][j]  / (self.min_cnt+self.absw[i][j]) )
                   else:
                      z.append ( self.yz[i][j]  / (self.min_cnt+self.absw[i][j]) )
               else:
                   z.append(np.float64('nan'))
        return xy,z

    
    def grid_corr(self):
        xy = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xy.append(self.i_to_ax1(i), self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xx[i][j] > self.x[i][j]**2 and self.w[i][j]*self.yy[i][j] > self.y[i][j]**2:
                  z.append ( (self.w[i][j]*self.xy[i][j] - self.xy[i][j]**2) / np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]**2) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]*self.y[i][j]) ) )
               else:
                  z.append(np.float64('nan'))
        return xy,z

    def corr(self,ax1,ax2):
        i = self.ax1_to_i(ax1)
        j = self.ax2_to_j(ax2)
        return ( (self.w[i][j]*self.xy[i][j] - self.x[i][j]*self.y[i][j]) /
                 np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]*self.x[i][j]) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]*self.y[i][j]) ) )

    def update(self, ax1,ax2,valz):#pyramid update
        #print ("\t\tUPDATE",ax1,ax2,valz)
        imin = max(1+self.ax1_to_i(ax1-self.ax1_ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+self.ax1_ks/2.0),self.ax1_pts-1)
        if imin == imax:
            hgt1 = 1.0
            imax += 1
        else:
            hgt1 = 1.5 / (imax-imin) #weight at peak

        jmin = max(1+self.ax2_to_j(ax2-self.ax2_ks/2.0),0)
        jmax = min(self.ax2_to_j(ax2+self.ax2_ks/2.0),self.ax2_pts-1)
        if jmin == jmax:
            hgt2 = 1.0
            jmax += 1
        else:
            hgt2 = 1.5 / (jmax-jmin) #weight at peak

        #print ("I:",imin,imax,"J:",jmin,jmax)
        asum = 0.0
        for i in range(imin,imax):
            for j in range(jmin,jmax):
                atten =  hgt1*hgt2*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /(self.ax1_ks+1) )*(1.0 - 2.0*abs(ax2 - self.j_to_ax2(j)) /(self.ax2_ks+1) )
                asum += atten
                #print (i, ax1,self.i_to_ax1(i),"\t", j, ax2,self.j_to_ax2(i),"\t", hgt1,hgt2,atten)
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks)
                self.x[i][j] += atten*ax1;
                self.y[i][j] += atten*ax2;
                self.xx[i][j] += atten*ax1*ax1;
                self.yy[i][j] += atten*ax2*ax2;
                self.xy[i][j] += atten*ax1*ax2;
                self.xz[i][j] += atten*ax1*valz;
                self.yz[i][j] += atten*ax2*valz;
                self.zz[i][j] += atten*valz*valz;
                self.w[i][j] += atten
                self.absw[i][j] += abs(atten)
        #print ("\t2D SUM ATTEN",ax1,ax2,asum)
        return
