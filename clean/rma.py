import numpy as np

#MOVING AVERAGE OF RATE.
#INTUITION MAKES IT LOOKS LIKE IT
#ISN'T WORKING RIGHT BUT I THINK
#IT IS.
class rma(object):
    def __init__(self, halflife):
        self.nd = np.exp(-np.log(2.0)/halflife)
        self.xsum = 0.0
        self.tsum = 0.0
        self.t_prev = -990.0

    def update(self, x,t):
        if self.t_prev < 0.0:
            self.t_prev = t
            self.xsum = x;
            return None
        dt = t - self.t_prev
        self.t_prev = t
        if dt == 0:
            self.xsum = self.xsum + x
        else:
            for iter in range(1,dt):
                self.xsum = self.nd*self.xsum 
                self.tsum = self.nd*self.tsum + 1
            self.xsum = self.nd*self.xsum + x
            self.tsum = self.nd*self.tsum + 1
        return self.xsum / self.tsum

    def last(self,t):
        dt = t - self.t_prev
        self.t_prev = t
        for iter in range(0,dt):
            self.xsum = self.nd*self.xsum 
            self.tsum = self.nd*self.tsum + dt
        return self.xsum / self.tsum


if __name__ == '__main__':

    #pass in the half-life for rate calculation
    X = rma(halflife=50)
    t,x = 30,22
    rv = X.update(x,t)
    if rv is None:
        print("event %.2f @ %.2f  RATE=NONE" % (x,t)  )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    x = 81
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    x = 8
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    x = 13.77
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
    t+=1
    x = 300
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
