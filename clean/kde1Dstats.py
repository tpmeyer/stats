#########################################
#
# SPACE is the X direction
# for a given X, there are values (x,y)
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# along the X direction
#
#########################################

import numpy as np
from numpy import random
import matplotlib.pyplot as plt

class kde1D(object):

    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,min_cnt=10):
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts)/(ax1_max-ax1_min)
       self.x =  np.zeros(ax1_pts) 
       self.xx = np.zeros(ax1_pts)
       self.y =  np.zeros(ax1_pts)
       self.yy = np.zeros(ax1_pts)
       self.xy = np.zeros(ax1_pts)
       self.w =  np.zeros(ax1_pts)
       self.inv = np.zeros(ax1_pts)

    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
   
    #don't bother interpolating, just use a fine enough grid...
    def grid_xy(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.xy[i] )
        return x,y

    def grid_cnt(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.w[i] )
        return x,y

    def grid_roi(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            if self.inv[i] > self.min_cnt:
               y.append ( self.xy[i]  / (self.min_cnt+self.inv[i]) )
            else:
               y.append(np.float64('nan'))
        return x,y

    def grid_corr(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            if self.w[i] > self.min_cnt and self.w[i]*self.xx[i] > self.x[i]*self.x[i] and self.w[i]*self.yy[i] > self.y[i]*self.y[i]:
               y.append ( (self.w[i]*self.xy[i] - self.x[i]*self.y[i]) /
                 np.sqrt( (self.w[i]*self.xx[i] - self.x[i]**2) * (self.w[i]*self.yy[i] - self.y[i]**2) ) )
            else:
               y.append(np.float64('nan'))
        return x,y

    

    def corr(self,val_ax1):
        i = self.ax1_to_i(val_ax1)
        return ( (self.w[i]*self.xy[i] - self.x[i]*self.y[i]) /
                 np.sqrt( (self.w[i]*self.xx[i] - self.x[i]**2) * (self.w[i]*self.yy[i] - self.y[i]**2) ) )
    # for update, the stats are going to be weird since 
    #each 'event' is spread over points, which should
    #sum to 1 to be able to normalize
    def vupdate(self, vax1,vvalx,vvaly):#triangle update
        if len(vax1) != len(vvalx) or len(vax1) != len(vvaly): return None
        for ctr in range(len(vax1)):
            print (self.ax1_min, self.ax1_max, vax1[ctr])
            self.update(vax1[ctr],vvalx[ctr],vvaly[ctr])
        return

    def update(self, ax1,valx,valy):#triangle update
        ks = self.ax1_ks
        imin = max(1+self.ax1_to_i(ax1-ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+ks/2.0),self.ax1_pts-1)
        if imax < imin:
            print ("kde1D sez:  dropped outside of grid.   Extend range and retry")
            quit()
        if imax == imin:
            print ("kde1D sez:  kernel fell between cracks.   Make it bigger and retry")
            quit()

        if imin == imax or imin + 1 == imax:
            self.x[imin] += valx;
            self.xx[imin] += valx*valx;
            self.w[imin] += 1.0
            #print ("TOO TIGHT",imin,imax,"ONE")
            return
        else:
            hgt = 2.0 / (imax-imin) #weight at peak
            #print (ax1,imin,imax,hgt)
            atten_tot = 0.0
            for i in range(imin,imax):
                atten =  hgt*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /ks )
                atten_tot += atten
            at2 = 0.0
            for i in range(imin,imax):
                atten =  hgt*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /ks ) / atten_tot
                if atten  < 0.0:
                    print ("CRAP",atten)
                    quit()
                at2 += atten
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /ks)
                self.x[i] += atten*valx;
                self.y[i] += atten*valy;
                self.xx[i] += atten*valx*valx;
                self.yy[i] += atten*valy*valy;
                self.xy[i] += atten*valx*valy;
                self.w[i] += atten
                self.inv[i] += abs(atten*valx)

            if abs(1.0 - at2) > 0.0001:
                print ("kde1D sez:  BAD NORM!")
                print("should be zero", abs(1.0 - at2) )
                quit()
            return

if __name__ == '__main__':

    ax1_min = -3
    ax1_max = 3
    ax1_pts = 10*(ax1_max - ax1_min)
    ax1_ks = 0.5
    g = kde1D( ax1_min,ax1_max,ax1_pts,ax1_ks,min_cnt=1)
    x,y =  0,0
    for i in range(500):
        loc = 3.0*random.rand() - 1
        g.update(loc,x,y)
        x += 1
        y += 1
    x,y =  0,0
    for i in range(500):
        loc = -3.0*random.rand() + 1
        g.update(loc,x,y)
        x += 1
        y -= 1

    x,y = g.grid_corr()
    print (y)
    plt.plot(x,y)
    plt.show()
                      
