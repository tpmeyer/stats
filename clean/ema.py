import numpy as np
class ema(object):

    def __init__(self, decay):
        self.nd = 1.0 - decay
        self.x = 0.0
        self.w = 0.0

    def update(self, x):
        self.x = self.nd*self.x + x
        self.w = self.nd*self.w +1.0
        return self.x / self.w

    def last(self):
        return self.x / self.w

if __name__ == '__main__':
    v = 1.0 - 1.0 / np.exp(0.1)

    X = ema(v)
    print (X.update(10.0) )
    print (X.update(11.0) )
    print (X.update(12.0) )
    print (X.update(13.0) )
    print (X.update(14.0) )
    print (X.update(15.0) )
    print (X.last() )
