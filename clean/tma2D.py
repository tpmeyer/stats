import numpy as np
from math import acos,pi
class tma2d(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
    def update(self,x,y,t):
       if (self.w < 0.0001): #first time through...
           self.age = t
           self.x= x
           self.y= y
           self.xx= x*x
           self.yy= y*y
           self.xy= x*y
           self.w = 1.0
           return
       else:
           
           z = (self.age-t) * self.decay  #negative number
           # taylor approximation less than 10^-4 error for abs(z) < 0.10
           if z > -0.01:
               #print ("approx this",np.exp(z),"with this",1+z+z*z/2.0)
               self.w *= (1.0 + z + z*z/2.0)
           else: self.w *= np.exp(z)

           self.x = (x + self.x * self.w) / (1.0+self.w)
           self.y = (y + self.y * self.w) / (1.0+self.w)
           self.xx = (x*x + self.xx * self.w) / (1.0+self.w)
           self.xy = (x*y + self.xy * self.w) / (1.0+self.w)
           self.yy = (y*y + self.yy * self.w) / (1.0+self.w)
           self.age = t
           self.w += 1.0

    def vx(self): return (self.xx - self.x*self.x)*self.w/(self.w-1.0)
    def vy(self): return (self.yy - self.y*self.y)*self.w/(self.w-1.0)
    def vxy(self): return (self.xy - self.x*self.y)*self.w/(self.w-1.0)

X = tma2d(0.05)
X.update(10.0,10.0,1.0)
X.update(11.0,11.0,1.1)
X.update(12.0,12.0,1.3)
X.update(13.0,13.0,1.50)
X.update(14.0,15.0,3.7)
X.update(15.0,10.0,3.8)
X.update(15.0,8.0,3.9)

#get the covariances:
C11=X.vx()
C22=X.vy()
C12=X.vxy()
#calc the lambdas:
lplus =  (C11 + C22 + np.sqrt( (C11-C22)*(C11-C22) + 4.0*C12*C12) ) / 2.0
lminus = (C11 + C22 - np.sqrt( (C11-C22)*(C11-C22) + 4.0*C12*C12) ) / 2.0
print ("C11,C22,C12",C11,C22,C12)
#calculate the eigenvectors:
V11 = C11 + C12 - lminus
V12 = C22 + C12 - lminus
V21 = C11 + C12 - lplus
V22 = C22 + C12 - lplus
#print ("ORIG: [",V11,V12,"][",V21,V22,"]")
#rotate to a consistent orientation:
if V11 < 0.0:
   V11 *= -1.0
   V12 *= -1.0
   #print ("FLIP Col1")
if V12 > 0.0:
   V12 *= -1.0
   V22 *= -1.0
   #print ("FLIP Row2")
if V21 > 0.0:
   V21 *= -1.0
   V22 *= -1.0
   #print ("FLIP Col2")
if V22 > 0.0:
   print ("not apposed to happen")
   print ("FAIL: [",V11,V12,"][",V21,V22,"]")
   quit()

#normalize
k = np.sqrt(V11*V11 + V12*V12)
Vplus = [V11/k,V12/k]
k = np.sqrt(V21*V21 + V22*V22)
Vminus = [V21/k,V22/k]
print ("eigenvectors",Vplus,Vminus )
print ("eigenvalues",lplus,lminus )
print ("pca angle is",180.0*acos(Vplus[0])/pi,"degrees" )

