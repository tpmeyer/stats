import numpy as np 

class tma(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.w = 0.0

    def update(self, x,t):
        if (self.w < 0.0001):
           self.age = t
           self.x= x
           self.w = 1.0
           return x
        else:
           up = (self.age-t) * self.decay  #negative number
           #print (up / self.decay)
           if up > -0.1:   # 10^-4 error
              self.w *= (1.0 + up + up*up/2.0)
           else:
              self.w *= np.exp(up)
        self.x = (x + self.x * self.w) / (1+self.w)
        self.age = t
        self.w += 1.0
        return self.x

    def last(self):
        return self.x

if __name__ == '__main__':

    v = 1.0 - 1.0 / np.exp(0.01)
    X = tma(0.999) 
    x = 10
    t = 1
    print (x,t,X.update(x,t) )
    x += 1
    t += 1
    print (x,t,X.update(x,t) )
    x += 1
    t += 1
    print (x,t,X.update(x,t) )
    x += 1
    t += 9
    print (x,t,X.update(x,t) )
    x += 0
    t += 1
    print (x,t,X.update(x,t) )
