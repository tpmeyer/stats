import numpy as np
class ema2d(object):

    def __init__(self, decay,icept=True):
        self.icept = icept
        self.nd = 1.0 - decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
        self.ww = 0.0

    def update(self, valx, valy):
        self.x = self.nd*self.x + valx;
        self.xx = self.nd*self.xx + valx*valx;
        self.y = self.nd*self.y + valy;
        self.yy = self.nd*self.yy + valy*valy;
        self.xy = self.nd*self.xy + valx*valy;
        self.w = self.nd*self.w + 1.0;
        self.ww = self.nd*self.ww + 1.0;


    def vx(self): return (self.xx*self.w - self.x*self.x) / (self.w*self.w-self.ww)
    def vy(self): return (self.yy*self.w - self.y*self.y) / (self.w*self.w-self.ww)
    def vxy(self): return (self.xy*self.w - self.x*self.y) / (self.w*self.w-self.ww)
    def beta(self):
       if self.icept:  
          XX = self.xx - self.x*self.x/self.w
          XY = self.xy - self.x*self.y/self.w
          return XY/XX if XX > 0.0 else 0.0
       else:
          return self.xy/self.xx if self.xx > 0.0 else 0.0
    def alpha(self):
       if self.icept:
          return  self.ybar() - self.beta()*self.xbar()
       else:
          return 0.0
    def xbar(self): return self.x / self.w
    def xxbar(self): return self.xx / self.w
    def ybar(self): return self.y / self.w
    def yybar(self): return self.yy / self.w
    def w(self): return self.w

if __name__ == '__main__':

    v = 0.1

    print ("Intercept = 0") 
    X = ema2d(v,icept=False)
    X.update(0,1)
    X.update(1,2)
    X.update(2,3)
    print ("C11=",X.vx()) 
    print ("C22=",X.vy() )
    print ("C12=C21=",X.vxy() )
    print ("xbar",X.xbar() )
    print ("ybar",X.ybar() )
    print ("alpha=",X.alpha())
    print ("beta=",X.beta())

    print ("\nIntercept = True") 
    X = ema2d(v,icept=True)
    X.update(0,1)
    X.update(1,2)
    X.update(2,3)
    print ("C11=",X.vx()) 
    print ("C22=",X.vy() )
    print ("C12=C21=",X.vxy() )
    print ("xbar",X.xbar() )
    print ("ybar",X.ybar() )
    print ("alpha=",X.alpha())
    print ("beta=",X.beta())
