#fix the x2i and i2x stuff so it matches np.linspace
#seems like trying to line up things for graphs
#and rounding is really hard.
#skip for now.

#########################################
#
# SPACE is the (X,Y) plane
# for a given (X,Y), there are values (A,B)
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# on (A,B)
# for every point on the (X,Y) plane
#
#########################################

import pandas as pd
import numpy as np
from numpy import random
import matplotlib.pyplot as plt


class kde2D(object):


    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,
                       ax2_min,ax2_max,ax2_pts,ax2_ks,
            min_cnt=10):
       xspc = (ax1_max-ax1_min+1)/ax1_pts
       yspc = (ax2_max-ax2_min+1)/ax2_pts
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts-1)/(ax1_max-ax1_min)
       self.ax2_min = ax2_min
       self.ax2_max = ax2_max
       self.ax2_pts = ax2_pts
       self.ax2_ks = ax2_ks
       self.ax2_scale = float(ax2_pts-1)/(ax2_max-ax2_min)
       self.x =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.xx =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.y =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.yy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.xy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.xxyy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.w =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.absx = np.zeros(shape=(ax1_pts,ax2_pts))
       #print (self.ax1_scale)
       


    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def ax2_to_j(self, ax2): return int(self.ax2_scale*(ax2-self.ax2_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
    def j_to_ax2(self, j): return float(j)/self.ax2_scale+self.ax2_min
   
    #don't botter interpolating, just use a fine enough grid...
    def get_xyw(self):
        return self.xgrid,self.ygrid,self.w
    def get_w(self):
        return self.w
    def cnt(self, x,y):
        i = self.ax1_to_i(x)
        j = self.ax1_to_i(y)
        return self.w[i][j]

    def grid_cnt(self):
        x = []
        y = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x.append( self.i_to_ax1(i) )
               y.append( self.j_to_ax2(j) )
               z.append( self.w[i][j] )
        return np.array(x),np.array(y),np.array(z)

    def list_roi(self):
        xy = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xy.append( (self.i_to_ax1(i), self.j_to_ax2(j) ) )
               if self.w[i][j] > self.min_cnt:
                   z.append ( self.xy[i][j]  / (self.min_cnt+self.absx[i][j]) )
               else:
                   z.append(np.float64('nan'))
        return xy,z

    
    def grid_Zscore(self):
        x_list = []
        y_list = []
        z_list = []
        rv =  np.zeros(shape=(self.ax1_pts,self.ax2_pts))
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x_list.append( self.i_to_ax1(i) )
               y_list.append( self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xxyy[i][j] > self.xy[i][j]**2:
                  z_list.append (np.sqrt(self.w[i][j]) * self.xy[i][j] / np.sqrt( self.w[i][j]*self.xxyy[i][j] - self.xy[i][j]*self.xy[i][j]) )
               else:
                  z_list.append( 0.0 )
        x_list = np.array(x_list)
        y_list = np.array(y_list)
        z_list = np.array(z_list)
        return x_list,y_list,z_list
        #N = int(len(z_list)**.5)
        z = z_list.reshape(self.ax1_pts, self.ax2_pts)
        zscope = max(abs(np.amin(z_list) ),np.amax(z_list))
        plt.imshow(z, cmap = 'RdBu', extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), aspect = 'auto', vmin=-zscope, vmax = zscope)
        plt.colorbar()
        plt.title("Z")
        plt.show()
        #print (x_list)
        #print (y_list)
        #for i in range(len(z_list)): print (i,z_list[i])
        return rv

    
    def grid_SR(self):
        x_list = []
        y_list = []
        z_list = []
        rv =  np.zeros(shape=(self.ax1_pts,self.ax2_pts))
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x_list.append( self.i_to_ax1(i) )
               y_list.append( self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xxyy[i][j] > self.xy[i][j]**2:
                  z_list.append ( self.xy[i][j] / np.sqrt( self.w[i][j]*self.xxyy[i][j] - self.xy[i][j]*self.xy[i][j]) )
               else:
                  z_list.append( 0.0 )
        x_list = np.array(x_list)
        y_list = np.array(y_list)
        z_list = np.array(z_list)
        return x_list,y_list,z_list

    def grid_roi(self):
        x_list = []
        y_list = []
        z_list = []
        rv =  np.zeros(shape=(self.ax1_pts,self.ax2_pts))
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x_list.append( self.i_to_ax1(i) )
               y_list.append( self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt:
                   rv[i][j] =  self.xy[i][j]  / (self.min_cnt+self.absx[i][j])
               else:
                   rv[i][j] = 0.0
               z_list.append( rv[i][j])
        x_list = np.array(x_list)
        y_list = np.array(y_list)
        z_list = np.array(z_list)
        return x_list,y_list,z_list
        #N = int(len(z_list)**.5)
        z = z_list.reshape(self.ax1_pts, self.ax2_pts)
        zscope = max(abs(np.amin(z_list) ),np.amax(z_list))
        plt.imshow(z, cmap = 'RdBu', extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), aspect = 'auto', vmin=-zscope, vmax = zscope)
        plt.colorbar()
        plt.title("ROI")
        plt.show()
        #print (x_list)
        #print (y_list)
        #for i in range(len(z_list)): print (i,z_list[i])
        return rv

    
    def grid_corr(self):
        x_list = []
        y_list = []
        z_list = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x_list.append( self.i_to_ax1(i) )
               y_list.append( self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xx[i][j] > self.x[i][j]**2 and self.w[i][j]*self.yy[i][j] > self.y[i][j]**2:
                  z_list.append ( ( (self.w[i][j]*self.xy[i][j] - self.x[i][j]*self.y[i][j]) /
                 np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]**2) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]**2) ) ) )

               else:
                  z_list.append(np.float64('nan'))
        x_list = np.array(x_list)
        y_list = np.array(y_list)
        z_list = np.array(z_list)
        #N = int(len(z_list)**.5)
        #z = z_list.reshape(N, N)
        z = z_list.reshape(self.ax1_pts, self.ax2_pts)
        plt.imshow(z,cmap = 'RdBu', extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), aspect = 'auto')
        plt.colorbar()
        plt.show()
        #print (x_list)
        #print (y_list)
        #for i in range(len(z_list)): print (i,z_list[i])
        
        return 
        
    def corr(self,ax1,ax2):
        i = self.ax1_to_i(ax1)
        j = self.ax2_to_j(ax2)
        if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xx[i][j] > self.x[i][j]**2 and self.w[i][j]*self.yy[i][j] > self.y[i][j]**2:
            return ( (self.w[i][j]*self.xy[i][j] - self.x[i][j]*self.y[i][j]) /
                 np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]*self.x[i][j]) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]*self.y[i][j]) ) )
        else: return 0.0
    # for update, the stats are going to be weird since 
    #each 'event' is spread over points, which should
    #sum to 1 to be able to normalize
    def update(self, ax1,ax2,valx,valy):#pyramid update
        #print ("\t\tUPDATE",ax1,ax2,valx,valy)
        imin = max(1+self.ax1_to_i(ax1-self.ax1_ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+self.ax1_ks/2.0),self.ax1_pts-1)
        if imin == imax:
            hgt1 = 1.0
        else:
            hgt1 = 2.0 / (imax-imin) #weight at peak

        jmin = max(1+self.ax2_to_j(ax2-self.ax2_ks/2.0),0)
        jmax = min(self.ax2_to_j(ax2+self.ax2_ks/2.0),self.ax2_pts-1)
        if jmin == jmax:
            hgt2 = 1.0
        else:
            hgt2 = 2.0 / (jmax-jmin) #weight at peak

        #print (imin,imax)
        #print (jmin,jmax)
        for i in range(imin,imax+1):
            for j in range(jmin,jmax+1):
                atten =  abs(hgt1*hgt2*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks )*(1.0 - 2.0*abs(ax2 - self.j_to_ax2(j)) /self.ax2_ks ))
                #print (i, ax1,self.i_to_ax1(i),"\t", j, ax2,self.j_to_ax2(i),"\t", hgt1,hgt2,atten)
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks)
                self.x[i][j] += atten*valx;
                self.y[i][j] += atten*valy;
                self.xx[i][j] += atten*valx*valx;
                self.yy[i][j] += atten*valy*valy;
                self.xy[i][j] += atten*valx*valy;
                self.xxyy[i][j] += atten*valx*valy*valx*valy;
                self.w[i][j] += atten
                self.absx[i][j] += abs(atten*valx)

            for j in range(jmin,jmax+1):
                atten =  abs(hgt1*hgt2*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks )*(1.0 - 2.0*abs(ax2 - self.j_to_ax2(j)) /self.ax2_ks ))
                #print (i, ax1,self.i_to_ax1(i),"\t", j, ax2,self.j_to_ax2(i),"\t", hgt1,hgt2,atten)
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks)
                self.x[i][j] += atten*valx;
                self.y[i][j] += atten*valy;
                self.xx[i][j] += atten*valx*valx;
                self.yy[i][j] += atten*valy*valy;
                self.xy[i][j] += atten*valx*valy;
                self.xxyy[i][j] += atten*valx*valy*valx*valy;
                self.w[i][j] += atten
                self.absx[i][j] += abs(atten*valx)

    def vupdate(self, ax1,ax2,valx,valy):#pyramid update
        if len(ax1) != len(ax2) or len(ax1) != len(ax2) or len(ax1) != len(ax2) or len(ax1) != len(ax2):
            print ("vupdate vectors len diff")
            print ("can't go on")
            quit()
        #ax1.to_csv("temp.csv")
        for ctr in range(len(ax1 )):
            #print( ax1[ctr], ax2[ctr], valx[ctr], valy[ctr] )
            if ctr % 1000 == 0: print (ctr,len(ax1))
            self.update( ax1[ctr], ax2[ctr], valx[ctr], valy[ctr] )


    def vpredict(self, ax1,ax2,valx, sgn=True): # if sgn==True set pred to zero when corr is negative
        rv = []
        if len(ax1) != len(ax2) or len(ax1) != len(ax2) or len(ax1) != len(ax2) or len(ax1) != len(ax2):
            print ("vupdate vectors len diff")
            print ("can't go on")
            quit()
        #ax1.to_csv("temp.csv")
        for ctr in range(len(ax1 )):
            r = self.corr( ax1[ctr], ax2[ctr])
            pred = 0.0
            if sgn == True:
                pred = r*valx[ctr] if r > 0.0 else 0.0
            else:
                pred = r*valx[ctr]
            rv.append(pred)
        return rv


if __name__ == '__main__':

    #td = pd.read_csv("../../infernet/options/TradingDates.csv")
    ax1_min = -3
    ax1_max = 3
    ax1_pts = 1*(ax1_max - ax1_min)
    ax1_ks = 1.0
    ax2_min = -3
    ax2_max = 3
    ax2_pts = 1*(ax2_max - ax2_min)
    ax2_ks = 1.0
    g = kde2D( ax1_min,ax1_max,ax1_pts,ax1_ks,
               ax2_min,ax2_max,ax2_pts,ax2_ks,min_cnt=0.1)
  
    g.update(2,1,2,2)
    g.update(2,1,2,2)
    g.update(2,1,2,2)
    y,x,z = g.grid_cnt()
    print (x)
    print (y)
    print(z)
    x = x.reshape(ax1_pts, ax2_pts)
    y = y.reshape(ax1_pts, ax2_pts)
    z = z.reshape(ax1_pts, ax2_pts)
    z = z[:-1, :-1]

    print (x)
    print (y)
    print(z)
    #z = z.reshape(ax1_pts, ax2_pts)
    plt.pcolormesh(x, y, z, cmap='RdBu')#, cmap=plt.cm.rainbow)#:w, vmax=zmax, vmin=zmin)
    plt.colorbar()
    plt.show()
    quit()
    zscope = max(abs(np.amin(z) ),np.amax(z))
    plt.imshow(z, cmap = 'RdBu', extent=(np.amin(x), np.amax(x), np.amin(y), np.amax(y)), aspect = 'auto', vmin=-zscope, vmax = zscope)
    plt.colorbar()
    plt.title(" Count ")
    plt.xlabel("X")
    plt.ylabel("Y")
    for x in np.arange(-2.5,2.5,0.1):
        print (x, g.cnt(x,1))
        print ("B",x, g.cnt(x,2))
    plt.show()
    quit()

    i = g.ax1_to_i(2)
    val = g.i_to_ax1(i)
    print ("2 >>",i,">>",val)
    j = g.ax2_to_j(-1)
    val = g.j_to_ax2(j)
    print ("-1 >>",j,">>",val)
    quit()
    C1 = 0.002
    C2 = 0.001
    C3 = 0.002
    C5 = 0.2
    pl = []
    pl2 = []
    inv = []
    rvec = []
    xsum = 0.0
    xxsum = 0.0
    for i in range(6000):
        loc1 = 5.0*random.rand() - 2.5
        loc2 = 5.0*random.rand() - 2.5
        signal = (C1 * loc1 + C2 * loc2)*10000
        ret = (C1*loc1 - C2*loc1*loc1 + C3 * loc2 + C2*loc2*loc2 + C5 * (random.rand()-0.5) )   * 10000.0
        g.update(loc1,loc2,signal,ret)
        #print (td.iloc[i].Date,loc1,loc2,signal,ret)
        x = ret*signal
        y = abs(signal)
        pl.append(x)
        pl2.append( x*x )
        inv.append(y)
        rvec.append(ret)
        xsum += x
        xxsum += x*x
       
    avg_pl =  np.array(pl).mean()
    std_pl =  np.array(pl).std()
    n = len(pl)
    s2 = np.sqrt( (n*xxsum - xsum*xsum ) / ((n-0)*(n-0)) )
    print ("BH SR",  np.array(rvec).mean() /  np.array(rvec).std() )
    print ("annualized SR of daily returns is ",16.0 * avg_pl / std_pl)
    print (std_pl,s2)
    g.grid_corr()
    g.grid_roi()

