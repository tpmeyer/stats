import numpy as np


class tmaStats(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
    def update(self,x,t):
       if (self.w < 0.0001): #first time through...
           self.age = t
           self.x= x
           self.xx= x*x
           self.w = 1.0
           return
       else:
           
           z = (self.age-t) * self.decay  #negative number
           # taylor approximation less than 10^-4 error for abs(z) < 0.10
           if z > -0.01:
               #print ("approx this",np.exp(z),"with this",1+z+z*z/2.0)
               self.w *= (1.0 + z + z*z/2.0)
           else: self.w *= np.exp(z)

           self.x = (x + self.x * self.w) / (1.0+self.w)
           self.xx = (x*x + self.xx * self.w) / (1.0+self.w)
           self.age = t
           self.w += 1.0
           return

    def x(self): return self.x/self.w
    def std(self): return np.sqrt(self.var() )
    def var(self): return (self.xx - self.x*self.x)*self.w/(self.w-1.0)

X = tmaStats(0.10)
for i in range(0,100):
   X.update(100.0,0 + float(i)/100.0)
print X.x,"+/-",X.std()

for i in range(0,100):
   X.update(100 + float(i)/100.0,100 + float(i)/100.0)
print X.x,"+/-",X.std()
