import numpy as np
from math import acos,pi
import time

test = 'ema'
class ema(object):

    def __init__(self, decay):
        self.nd = 1.0 - decay
        self.x = 0.0
        self.xx = 0.0
        self.w = 0.0
        self.ww = 0.0

    def vupdate(self, vecx):
        for valx in vecx:
            self.update(valx)
    def update(self, valx):
        self.x = self.nd*self.x + valx
        self.xx = self.nd*self.xx + valx*valx
        self.w = self.nd*self.w +1.0
        self.ww = self.nd*self.ww + 1.0
        return self.x / self.w

    def mean(self):
        return self.x / self.w
    def cnt(self):
        return self.w
    def var(self):
        if self.w > 1.0:
            return (self.xx*self.w - self.x*self.x) / (self.w*self.w-self.ww)
        else:
            return 999
    def std(self):
        return np.sqrt( self.var() )


class ema2d(object):

    def __init__(self, decay,icept=True):
        self.icept = icept
        self.nd = 1.0 - decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
        self.ww = 0.0

    def update(self, valx, valy):
        self.x = self.nd*self.x + valx
        self.xx = self.nd*self.xx + valx*valx
        self.y = self.nd*self.y + valy
        self.yy = self.nd*self.yy + valy*valy
        self.xy = self.nd*self.xy + valx*valy
        self.w = self.nd*self.w + 1.0
        self.ww = self.nd*self.ww + 1.0


    def vx(self): return (self.xx*self.w - self.x*self.x) / (self.w*self.w-self.ww)
    def vy(self): return (self.yy*self.w - self.y*self.y) / (self.w*self.w-self.ww)
    def vxy(self): return (self.xy*self.w - self.x*self.y) / (self.w*self.w-self.ww)
    def beta(self):
       if self.icept:  
          XX = self.xx - self.x*self.x/self.w
          XY = self.xy - self.x*self.y/self.w
          return XY/XX if XX > 0.0 else 0.0
       else:
          return self.xy/self.xx if self.xx > 0.0 else 0.0
    def alpha(self):
       if self.icept:
          return  self.ybar() - self.beta()*self.xbar()
       else:
          return 0.0
    def xbar(self): return self.x / self.w
    def xxbar(self): return self.xx / self.w
    def ybar(self): return self.y / self.w
    def yybar(self): return self.yy / self.w
    def w(self): return self.w


class tma(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.w = 0.0

    def update(self, x,t = None):
        if t == None:
           t = time.time()
        if (self.w < 0.0001):
           self.age = t
           self.x= x
           self.w = 1.0
           return x
        else:
           up = (self.age-t) * self.decay  #negative number
           #print (up / self.decay)
           if up > -0.1:   # 10^-4 error
              self.w *= (1.0 + up + up*up/2.0)
           else:
              self.w *= np.exp(up)
        self.x = (x + self.x * self.w) / (1+self.w)
        self.age = t
        self.w += 1.0
        return self.x

    def last(self):
        return self.x

class tma2d(object):

    def __init__(self, decay):
        self.decay = decay
        self.x = 0.0
        self.xx = 0.0
        self.xy = 0.0
        self.y = 0.0
        self.yy = 0.0
        self.w = 0.0
    def update(self,x,y,t):
       if (self.w < 0.0001): #first time through...
           self.age = t
           self.x= x
           self.y= y
           self.xx= x*x
           self.yy= y*y
           self.xy= x*y
           self.w = 1.0
           return
       else:
           
           z = (self.age-t) * self.decay  #negative number
           # taylor approximation less than 10^-4 error for abs(z) < 0.10
           if z > -0.01:
               #print ("approx this",np.exp(z),"with this",1+z+z*z/2.0)
               self.w *= (1.0 + z + z*z/2.0)
           else: self.w *= np.exp(z)

           self.x = (x + self.x * self.w) / (1.0+self.w)
           self.y = (y + self.y * self.w) / (1.0+self.w)
           self.xx = (x*x + self.xx * self.w) / (1.0+self.w)
           self.xy = (x*y + self.xy * self.w) / (1.0+self.w)
           self.yy = (y*y + self.yy * self.w) / (1.0+self.w)
           self.age = t
           self.w += 1.0

    def vx(self): return (self.xx - self.x*self.x)*self.w/(self.w-1.0)
    def vy(self): return (self.yy - self.y*self.y)*self.w/(self.w-1.0)
    def vxy(self): return (self.xy - self.x*self.y)*self.w/(self.w-1.0)

#MOVING AVERAGE OF RATE.
#INTUITION MAKES IT LOOKS LIKE IT
#ISN'T WORKING RIGHT BUT I THINK
#IT IS.
#only works with integer time increments.
#someone should fix this...
class rma(object):
    def __init__(self, halflife):
        self.nd = np.exp(-np.log(2.0)/halflife)
        self.xsum = 0.0
        self.tsum = 0.0000001
        self.t_prev = -990.0

    def update(self, x,t = None):
        if t == None:
           t = int( time.time() )
        if self.t_prev < 0.0:
            self.t_prev = t
            self.xsum = x
            return None
        dt = int(t - self.t_prev)
        self.t_prev = t
        if dt == 0:
            self.xsum = self.xsum + x
        else:
            for iter in range(1,dt):
                self.xsum = self.nd*self.xsum 
                self.tsum = self.nd*self.tsum + 1
            self.xsum = self.nd*self.xsum + x
            self.tsum = self.nd*self.tsum + 1
        return self.xsum / self.tsum

    def last(self,t = None):
        if t == None:
           t = int(time.time() )
        dt = t - self.t_prev
        self.t_prev = t
        for iter in range(0,dt):
            self.xsum = self.nd*self.xsum 
            self.tsum = self.nd*self.tsum + dt
        return self.xsum / self.tsum


if __name__ == '__main__':

  if test == "ema":
    #pass in the half-life for rate calculation
    X = ema(decay = 0.1)
    for i in range(10):
       X.update(i)
       print (i,X.mean(),X.std())
  if test == "rma":
    #pass in the half-life for rate calculation
    X = rma(halflife=50)
    t,x = 30,22
    rv = X.update(x,t)
    if rv is None:
        print("event %.2f @ %.2f  RATE=NONE" % (x,t)  )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    x = 81
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    x = 8
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
    t+=1
    print("t = %.2f  RATE=%.2f" % (t, X.last(t) ) )
    t+=1
    x = 13.77
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )
    t+=1
    x = 300
    print("event %.2f @ %.2f  RATE=%.2f" % (x,t, X.update(x,t) ) )

  if test == "ema2D":
    v = 1.0 - 1.0 / np.exp(0.1)

    X = ema(v)
    print (X.update(10.0) )
    print (X.update(11.0) )
    print (X.update(12.0) )
    print (X.update(13.0) )
    print (X.update(14.0) )
    print (X.update(15.0) )
    print (X.last() )


  if test == "ema2D":

    v = 0.1

    print ("Intercept = 0") 
    X = ema2d(v,icept=False)
    X.update(0,1)
    X.update(1,2)
    X.update(2,3)
    print ("C11=",X.vx()) 
    print ("C22=",X.vy() )
    print ("C12=C21=",X.vxy() )
    print ("xbar",X.xbar() )
    print ("ybar",X.ybar() )
    print ("alpha=",X.alpha())
    print ("beta=",X.beta())

    print ("\nIntercept = True") 
    X = ema2d(v,icept=True)
    X.update(0,1)
    X.update(1,2)
    X.update(2,3)
    print ("C11=",X.vx()) 
    print ("C22=",X.vy() )
    print ("C12=C21=",X.vxy() )
    print ("xbar",X.xbar() )
    print ("ybar",X.ybar() )
    print ("alpha=",X.alpha())
    print ("beta=",X.beta())


  if test == "tma":

    v = 1.0 - 1.0 / np.exp(0.01)
    X = tma(0.999) 
    x = 10
    t = 1
    print (x,t,X.update(x,t) )
    x += 1
    t += 1
    print (x,t,X.update(x,t) )
    x += 1
    t += 1
    print (x,t,X.update(x,t) )
    x += 1
    t += 9
    print (x,t,X.update(x,t) )
    x += 0
    t += 1
    print (x,t,X.update(x,t) )


  if test == "tma2d":
    X = tma2d(0.05)
    X.update(10.0,10.0,1.0)
    X.update(11.0,11.0,1.1)
    X.update(12.0,12.0,1.3)
    X.update(13.0,13.0,1.50)
    X.update(14.0,15.0,3.7)
    X.update(15.0,10.0,3.8)
    X.update(15.0,8.0,3.9)
    
    #get the covariances:
    C11=X.vx()
    C22=X.vy()
    C12=X.vxy()
    #calc the lambdas:
    lplus =  (C11 + C22 + np.sqrt( (C11-C22)*(C11-C22) + 4.0*C12*C12) ) / 2.0
    lminus = (C11 + C22 - np.sqrt( (C11-C22)*(C11-C22) + 4.0*C12*C12) ) / 2.0
    print ("C11,C22,C12",C11,C22,C12)
    #calculate the eigenvectors:
    V11 = C11 + C12 - lminus
    V12 = C22 + C12 - lminus
    V21 = C11 + C12 - lplus
    V22 = C22 + C12 - lplus
    #print ("ORIG: [",V11,V12,"][",V21,V22,"]")
    #rotate to a consistent orientation:
    if V11 < 0.0:
       V11 *= -1.0
       V12 *= -1.0
       #print ("FLIP Col1")
    if V12 > 0.0:
       V12 *= -1.0
       V22 *= -1.0
       #print ("FLIP Row2")
    if V21 > 0.0:
       V21 *= -1.0
       V22 *= -1.0
       #print ("FLIP Col2")
    if V22 > 0.0:
       print ("not apposed to happen")
       print ("FAIL: [",V11,V12,"][",V21,V22,"]")
       quit()

    #normalize
    k = np.sqrt(V11*V11 + V12*V12)
    Vplus = [V11/k,V12/k]
    k = np.sqrt(V21*V21 + V22*V22)
    Vminus = [V21/k,V22/k]
    print ("eigenvectors",Vplus,Vminus )
    print ("eigenvalues",lplus,lminus )
    print ("pca angle is",180.0*acos(Vplus[0])/pi,"degrees" )

