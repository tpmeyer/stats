#########################################
#
# SPACE is the X direction
# for a given X, there are values (x,y)
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# along the X direction
#
#########################################

import numpy as np
from numpy import random
import matplotlib.pyplot as plt

EPS = 1e-9 #to put in denominators to avoid inf

class kde1D(object):

    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,min_cnt=10):
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts)/(ax1_max-ax1_min)
       self.x =  np.zeros(ax1_pts) 
       self.xx = np.zeros(ax1_pts)
       self.w =  np.zeros(ax1_pts)

    def ax1_to_i(self, ax1):
        print ("AX1",ax1)
        return int(self.ax1_scale*(ax1-self.ax1_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min

    def std(self): return np.sqrt(self.var() )
    def var(self):
        i = ax1_to_i(x)
        return (self.xx[i]*self.w[i] - self.x[i]*self.x[i])

    #don't bother interpolating, just use a fine enough grid...
    def copy(self):
        rv = kde1D(self.ax1_min,self.ax1_max,self.ax1_pts,self.ax1_ks,self.min_cnt)
        rv.x = self.x  
        rv.xx = self.xx 
        rv.e = self.w
        return rv

    def grid_sum(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.x[i] )
        return x,y

    def grid_mean(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.x[i] / (EPS+self.w[i])  )
        return x,y

    def grid_cnt(self):
        x = []
        y = []
        for i in range(self.ax1_pts):
            x.append(self.i_to_ax1(i) )
            y.append( self.w[i] )
        return x,y

    # for update, the stats are going to be weird since 
    #each 'event' is spread over points, which should
    #sum to 1 to be able to normalize
    def update(self, ax1,valx):
        ks = self.ax1_ks

        print ("UP",ax1,ks,valx)
        imin = max(1+self.ax1_to_i(ax1-ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+ks/2.0),self.ax1_pts-1)
        if imax < imin:
            print ("kde1D sez:  dropped outside of grid.   Extend range and retry")
            quit()
        if imax == imin:
            print ("kde1D sez:  kernel fell between cracks.   Make it bigger and retry")
            quit()

        if imin == imax or imin + 1 == imax:
            self.x[imin] += valx;
            self.xx[imin] += valx*valx;
            self.w[imin] += 1.0
            #print ("TOO TIGHT",imin,imax,"ONE")
            return
        else:
            hgt = 2.0 / (imax-imin) #weight at peak
            #print (ax1,imin,imax,hgt)
            atten_tot = 0.0
            for i in range(imin,imax):
                atten =  hgt*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /ks )
                atten_tot += atten
            at2 = 0.0
            for i in range(imin,imax):
                atten =  hgt*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /ks ) / atten_tot
                if atten  < 0.0:
                    print ("CRAP",atten, ax1,atten_to_valt)
                    atten = 0.0
                    #quit()
                at2 += atten
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /ks)
                self.x[i] += atten*valx;
                self.xx[i] += atten*valx*valx;
                self.w[i] += atten
            if abs(1.0 - at2) > 0.0001:
                print ("kde1D sez:  BAD NORM!")
                print("should be zero", abs(1.0 - at2) )
                quit()
            return
    
if __name__ == '__main__':

    ax1_min = -3
    ax1_max = 3
    ax1_pts = 50*(ax1_max - ax1_min)
    ax1_ks = 0.5
    g = kde1D( ax1_min,ax1_max,ax1_pts,ax1_ks,min_cnt=1)
    g.update(2.0,1.0)
    g.update(1.0,-0.5)
    a,b = g.grid_sum()
    plt.plot(a,b)
    g.update(2.1,0.5)
    g.update(2.2,-0.5)
    a,b = g.grid_sum()
    plt.plot(a,b)
    plt.show()
