
#########################################
#
# SPACE is the (X,Y) plane
# for a given (X,Y), there are values (A,B)
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# on (A,B)
# for every point on the (X,Y) plane
#
#########################################

import pandas as pd
import numpy as np
from numpy import random
import matplotlib.pyplot as plt


class kde2D(object):


    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,
                       ax2_min,ax2_max,ax2_pts,ax2_ks,
            min_cnt=10):
       xspc = (ax1_max-ax1_min+1)/ax1_pts
       yspc = (ax2_max-ax2_min+1)/ax2_pts
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts-1)/(ax1_max-ax1_min)
       self.ax2_min = ax2_min
       self.ax2_max = ax2_max
       self.ax2_pts = ax2_pts
       self.ax2_ks = ax2_ks
       self.ax2_scale = float(ax2_pts-1)/(ax2_max-ax2_min)
       self.x =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.xx =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.y =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.yy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.xy =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.w =    np.zeros(shape=(ax1_pts,ax2_pts))
       self.absw = np.zeros(shape=(ax1_pts,ax2_pts))
       self.absx = np.zeros(shape=(ax1_pts,ax2_pts))
       #print (self.ax1_scale)
       


    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def ax2_to_j(self, ax2): return int(self.ax2_scale*(ax2-self.ax2_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
    def j_to_ax2(self, j): return float(j)/self.ax2_scale+self.ax2_min
   
    #don't botter interpolating, just use a fine enough grid...
    def get_xyw(self):
        return self.xgrid,self.ygrid,self.w
    def get_w(self):
        return self.w
    def grid_cnt(self):
        xyz = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xyz.append( ( (self.i_to_ax1(i), self.j_to_ax2(j) ) , self.w[i][j] ) )
        return xyz

    def list_roi(self):
        xy = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xy.append( (self.i_to_ax1(i), self.j_to_ax2(j) ) )
               if self.absw[i][j] > self.min_cnt:
                   z.append ( self.xy[i][j]  / (self.min_cnt+self.absx[i][j]) )
               else:
                   z.append(np.float64('nan'))
        return xy,z

    def roi(self,ax1,ax2):
        i = self.ax1_to_i(ax1)
        j = self.ax2_to_j(ax2)
        if self.absw[i][j] > self.min_cnt:
            return  self.xy[i][j]  / (self.min_cnt+self.absx[i][j])
        else:
            return 0.0

    def grid_roi(self):
        x_list = []
        y_list = []
        z_list = []
        rv =  np.zeros(shape=(self.ax1_pts,self.ax2_pts))
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x_list.append( self.i_to_ax1(i) )
               y_list.append( self.j_to_ax2(j) )
               if self.absw[i][j] > self.min_cnt:
                   rv[i][j] =  self.xy[i][j]  / (self.min_cnt+self.absx[i][j])
               else:
                   rv[i][j] = 0.0
               z_list.append( rv[i][j])
        x_list = np.array(x_list)
        y_list = np.array(y_list)
        z_list = np.array(z_list)
        N = int(len(z_list)**.5)
        z = z_list.reshape(N, N)
        zscope = max(abs(np.amin(z_list) ),np.amax(z_list))
        plt.imshow(z, cmap = 'RdBu', extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), aspect = 'auto', vmin=-zscope, vmax = zscope)
        plt.colorbar()
        plt.title("ROI")
        plt.show()
        #print (x_list)
        #print (y_list)
        #for i in range(len(z_list)): print (i,z_list[i])
        return rv

    
    def grid_corr(self):
        x_list = []
        y_list = []
        z_list = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               x_list.append( self.i_to_ax1(i) )
               y_list.append( self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xx[i][j] > self.x[i][j]**2 and self.w[i][j]*self.yy[i][j] > self.y[i][j]**2:
                  z_list.append ( ( (self.w[i][j]*self.xy[i][j] - self.x[i][j]*self.y[i][j]) /
                 np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]**2) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]**2) ) ) )

               else:
                  z_list.append(np.float64('nan'))
        x_list = np.array(x_list)
        y_list = np.array(y_list)
        z_list = np.array(z_list)
        N = int(len(z_list)**.5)
        z = z_list.reshape(N, N)
        zscope = max(abs(np.amin(z_list) ),np.amax(z_list))
        plt.imshow(z, cmap = 'RdBu', extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), aspect = 'auto', vmin=-zscope, vmax = zscope)
        #plt.imshow(z,cmap = 'RdBu', extent=(np.amin(x_list), np.amax(x_list), np.amin(y_list), np.amax(y_list)), aspect = 'auto')
        plt.colorbar()
        plt.title("correlation")
        plt.show()
        #print (x_list)
        #print (y_list)
        #for i in range(len(z_list)): print (i,z_list[i])
        
        return 
        
    def corr(self,ax1,ax2):
        i = self.ax1_to_i(ax1)
        j = self.ax2_to_j(ax2)
        return ( (self.w[i][j]*self.xy[i][j] - self.x[i][j]*self.y[i][j]) /
                 np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]*self.x[i][j]) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]*self.y[i][j]) ) )
    # for update, the stats are going to be weird since 
    #each 'event' is spread over points, which should
    #sum to 1 to be able to normalize
    def update(self, ax1,ax2,valx,valy):#pyramid update
        #print ("\t\tUPDATE",ax1,ax2,valx,valy)
        imin = max(1+self.ax1_to_i(ax1-self.ax1_ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+self.ax1_ks/2.0),self.ax1_pts-1)
        if imin == imax:
            hgt1 = 1.0
        else:
            hgt1 = 2.0 / (imax-imin) #weight at peak

        jmin = max(1+self.ax2_to_j(ax2-self.ax2_ks/2.0),0)
        jmax = min(self.ax2_to_j(ax2+self.ax2_ks/2.0),self.ax2_pts-1)
        if jmin == jmax:
            hgt2 = 1.0
        else:
            hgt2 = 2.0 / (jmax-jmin) #weight at peak

        #print (imin,imax)
        #print (jmin,jmax)
        for i in range(imin,imax+1):
            for j in range(jmin,jmax+1):
                atten =  hgt1*hgt2*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks )*(1.0 - 2.0*abs(ax2 - self.j_to_ax2(j)) /self.ax2_ks )
                #print (i, ax1,self.i_to_ax1(i),"\t", j, ax2,self.j_to_ax2(i),"\t", hgt1,hgt2,atten)
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks)
                self.x[i][j] += atten*valx;
                self.y[i][j] += atten*valy;
                self.xx[i][j] += atten*valx*valx;
                self.yy[i][j] += atten*valy*valy;
                self.xy[i][j] += atten*valx*valy;
                self.w[i][j] += atten
                self.absx[i][j] += abs(atten*valx)
                self.absw[i][j] += abs(atten)


#LOAD IN A SIGNAL (independent var)
# AND PRODUCT FUTURE RETURN (depenedent VAR)
#ALONG WITH TWO INDEPENDENT VARIABLES
#FEED THE RESULTS INTO A KDE AND
#WALK IT FORWARD ONLY TRADING THE 
#REGIONS WITH POSITIVE ROI
#COMPARE WITH UNFILTERED
if __name__ == '__main__':

    df = pd.read_csv("if3.csv",index_col = 'Date')
    print (df.corr())
    df = df.head(1500).tail(1000)
    print (df.corr())
    ax1_min = -3
    ax1_max = 3
    ax1_pts = 10*(ax1_max - ax1_min)
    ax1_ks = 4.5
    ax2_min = -3
    ax2_max = 3
    ax2_pts = 10*(ax1_max - ax1_min)
    ax2_ks = 4.5
    g = kde2D( ax1_min,ax1_max,ax1_pts,ax1_ks,
               ax2_min,ax2_max,ax2_pts,ax2_ks,min_cnt=0.1)

    df['PL'] = df.signal * df.ret
    print (df.head())
    print (df.tail())
  
    pl = []
    pl2 = []
    inv = []
    rvec = []
    xsum = 0.0
    xxsum = 0.0
    filtervec = []
    A = 0.3
    atten = 1000.0
    for index, row in df.iterrows():
        loc1 = row.X1
        loc2 = row.X2
        signal = row.signal
        ret = row.ret
        fil = 1.0 + A * np.tanh(g.roi(loc1,loc2)*atten )
        filtervec.append(fil)
        g.update(loc1,loc2,signal,ret)
        #print (index,fil)
     
    df['sfac'] = filtervec
    print ("STD ATTEN",df.sfac.std())
    df['PLfilter'] = df.signal * df.ret * df.sfac
    #print ("BH SR",  df.ret.mean() /  df.ret.std() ) 
    print ("annualized SR/ ROI  of daily returns is ",16.0 * df.PL.mean() /  df.PL.std() ,df.PL.sum() /  sum(abs(df.signal) )  )
    print ("filtered SR/ ROI  of daily returns is ",16.0 * df.PLfilter.mean() /  df.PLfilter.std()  ,df.PLfilter.sum() /  sum(abs(df.signal*df.sfac) )  )
    g.grid_corr()
    g.grid_roi()
    df[ [ 'PL','PLfilter'] ].cumsum().plot()
    plt.title("Equity Curve")
    plt.show()
