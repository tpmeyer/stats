#fix the x2i and i2x stuff so it matches np.linspace
#seems like trying to line up things for graphs
#and rounding is really hard.
#skip for now.
#pyramid update
#########################################
#
# SPACE is the (X,Y) plane
# for a given (X,Y), there is a value Z
# we will create kdeStats (beta,corr,Sharpe Ratio,etc.)
# for every point on the (X,Y) plane
#
#########################################

import numpy as np
class kde2D(object):


    def __init__(self, ax1_min,ax1_max,ax1_pts,ax1_ks,
                       ax2_min,ax2_max,ax2_pts,ax2_ks,
            min_cnt=10):
       xspc = (ax1_max-ax1_min+1)/ax1_pts
       yspc = (ax2_max-ax2_min+1)/ax2_pts
       self.min_cnt = min_cnt
       self.ax1_min = ax1_min
       self.ax1_max = ax1_max
       self.ax1_pts = ax1_pts
       self.ax1_ks = ax1_ks
       self.ax1_scale = float(ax1_pts-1)/(ax1_max-ax1_min)
       self.ax2_min = ax2_min
       self.ax2_max = ax2_max
       self.ax2_pts = ax2_pts
       self.ax2_ks = ax2_ks
       self.ax2_scale = float(ax2_pts-1)/(ax2_max-ax2_min)
       self.x =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.xx =  np.zeros(shape=(ax1_pts,ax2_pts))
       self.y =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.yy =  np.zeros(shape=(ax1_pts,ax2_pts))
       self.xy =  np.zeros(shape=(ax1_pts,ax2_pts))
       self.xz =  np.zeros(shape=(ax1_pts,ax2_pts))
       self.yz =  np.zeros(shape=(ax1_pts,ax2_pts))
       self.zz =  np.zeros(shape=(ax1_pts,ax2_pts))
       self.w =   np.zeros(shape=(ax1_pts,ax2_pts))
       self.absw = np.zeros(shape=(ax1_pts,ax2_pts))
       #print (self.ax1_scale)
       


    def ax1_to_i(self, ax1): return int(self.ax1_scale*(ax1-self.ax1_min) )
    def ax2_to_j(self, ax2): return int(self.ax2_scale*(ax2-self.ax2_min) )
    def i_to_ax1(self, i): return float(i)/self.ax1_scale+self.ax1_min
    def j_to_ax2(self, j): return float(j)/self.ax2_scale+self.ax2_min
   
    #don't botter interpolating, just use a fine enough grid...
    def get_xyw(self):
        return self.xgrid,self.ygrid,self.w
    def get_w(self):
        return self.w
    def grid_cnt(self):
        xyz = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xyz.append( ( (self.i_to_ax1(i), self.j_to_ax2(j) ) , self.w[i][j] ) )
        return xyz
    def grid_roi(self):
        rv =  np.zeros(shape=(self.ax1_pts,self.ax2_pts))
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               if self.absw[i][j] > self.min_cnt:
                   rv[i][j] =  self.xy[i][j]  / (self.min_cnt+self.absw[i][j])
               else:
                   rv[i][j] = 0.0
        return rv

    def list_roi(self):
        xy = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xy.append( (self.i_to_ax1(i), self.j_to_ax2(j) ) )
               if self.absw[i][j] > self.min_cnt:
                   z.append ( self.xy[i][j]  / (self.min_cnt+self.absw[i][j]) )
               else:
                   z.append(np.float64('nan'))
        return xy,z

    
    def grid_corr(self):
        xy = []
        z = []
        for i in range(self.ax1_pts):
           for j in range(self.ax2_pts):
               xy.append(self.i_to_ax1(i), self.j_to_ax2(j) )
               if self.w[i][j] > self.min_cnt and self.w[i][j]*self.xx[i][j] > self.x[i][j]**2 and self.w[i][j]*self.yy[i][j] > self.y[i][j]**2:
                  z.append ( (self.w[i][j]*self.xy[i][j] - self.xy[i][j]**2) / np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]**2) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]*self.y[i][j]) ) )
               else:
                  z.append(np.float64('nan'))
        return xy,z

    

    def corr(self,ax1,ax2):
        i = self.ax1_to_i(ax1)
        j = self.ax2_to_j(ax2)
        return ( (self.w[i][j]*self.xy[i][j] - self.x[i][j]*self.y[i][j]) /
                 np.sqrt( (self.w[i][j]*self.xx[i][j] - self.x[i][j]*self.x[i][j]) * (self.w[i][j]*self.yy[i][j] - self.y[i][j]*self.y[i][j]) ) )
    # for update, the stats are going to be weird since 
    #each 'event' is spread over points, which should
    #sum to 1 to be able to normalize
    def update(self, ax1,ax2,valz):#pyramid update
        print ("\t\tUPDATE",ax1,ax2,valz)
        imin = max(1+self.ax1_to_i(ax1-self.ax1_ks/2.0),0)
        imax = min(self.ax1_to_i(ax1+self.ax1_ks/2.0),self.ax1_pts-1)
        if imin == imax:
            hgt1 = 1.0
        else:
            hgt1 = 2.0 / (imax-imin) #weight at peak

        jmin = max(1+self.ax2_to_j(ax2-self.ax2_ks/2.0),0)
        jmax = min(self.ax2_to_j(ax2+self.ax2_ks/2.0),self.ax2_pts-1)
        if jmin == jmax:
            hgt2 = 1.0
        else:
            hgt2 = 2.0 / (jmax-jmin) #weight at peak

        print (imin,imax)
        #print (jmin,jmax)
        for i in range(imin,imax+1):
            for j in range(jmin,jmax+1):
                atten =  hgt1*hgt2*(1.0 - 2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks )*(1.0 - 2.0*abs(ax2 - self.j_to_ax2(j)) /self.ax2_ks )
                print (i, ax1,self.i_to_ax1(i),"\t", j, ax2,self.j_to_ax2(i),"\t", hgt1,hgt2,atten)
                #print ("\t\t",i, ax1, self.i_to_ax1(i),2.0*abs(ax1 - self.i_to_ax1(i)) /self.ax1_ks)
                self.x[i][j] += atten*ax1;
                self.y[i][j] += atten*ax2;
                self.xx[i][j] += atten*ax1*ax1;
                self.yy[i][j] += atten*ax2*ax2;
                self.xy[i][j] += atten*ax1*ax2;
                self.xz[i][j] += atten*ax1*valz;
                self.yz[i][j] += atten*ax2*valz;
                self.zz[i][j] += atten*valz*valz;
                self.w[i][j] += atten
                self.absw[i][j] += abs(atten)
