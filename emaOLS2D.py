import numpy as np
class emaols2d(object):

    def __init__(self, decay,icept=True):
       
        self.icept = icept
        self.nd = 1.0 - decay
        self.x = 0.0
        self.y = 0.0
        self.xx = 0.0
        self.z = 0.0
        self.zz = 0.0
        self.xy = 0.0
        self.xz = 0.0
        self.yz = 0.0
        self.w = 0.0
        self.ww = 0.0

    def update(self, valy, valx,valz):
        self.x = self.nd*self.x + valx;
        self.y = self.nd*self.y + valy;
        self.z = self.nd*self.z + valz;
        self.xx = self.nd*self.xx + valx*valx;
        self.zz = self.nd*self.zz + valz*valz;
        self.xy = self.nd*self.xy + valx*valy;
        self.xz = self.nd*self.xz + valx*valz;
        self.yz = self.nd*self.yz + valy*valz;
        self.w = self.nd*self.w + 1.0;
        self.ww = self.nd*self.ww + 1.0;


    def vx(self): return (self.xx*self.w - self.x*self.x) / (self.w*self.w-self.ww)
    def vz(self): return (self.zz*self.w - self.z*self.z) / (self.w*self.w-self.ww)
    def alpha(self):
       if self.icept:
          return self.ybar() - self.betax()*self.xbar()- self.betaz()*self.zbar()
       else:
          return 0.0
    def betax(self):
       if self.icept:
          XX = self.xx - self.x*self.x/self.w
          XY = self.xy - self.x*self.y/self.w
          YZ = self.yz - self.y*self.z/self.w
          XZ = self.xz - self.x*self.z/self.w
          ZZ = self.zz - self.z*self.z/self.w
       else:
          XX = self.xx
          XY = self.xy
          YZ = self.yz
          XZ = self.xz
          ZZ = self.zz
       return (ZZ*XY -XZ*YZ)/(ZZ*XX -XZ*XZ) if ZZ > 0.0 else 0
    def betaz(self):
       if self.icept:
          XX = self.xx - self.x*self.x/self.w
          ZZ = self.zz - self.z*self.z/self.w
          XY = self.xy - self.x*self.y/self.w
          XZ = self.xz - self.x*self.z/self.w
          YZ = self.yz - self.y*self.z/self.w
       else:
          XX = self.xx
          ZZ = self.zz
          XY = self.xy
          XZ = self.xz
          YZ = self.yz
       return (XX*YZ -XZ*XY)/(ZZ*XX -XZ*XZ) if XX > 0.0 else 0
    def xbar(self): return self.x / self.w
    def ybar(self): return self.y / self.w
    def xxbar(self): return self.xx / self.w
    def zbar(self): return self.z / self.w
    def zzbar(self): return self.zz / self.w
    def w(self): return self.w

v = 0.0

ic = emaols2d(v,True)
noi = emaols2d(v,False)
x = 3
z = 2
y = 2*x +3*z - 5
ic.update(y,x,z)
noi.update(y,x,z)

x = 1
z = 3
y = 2*x +3*z - 5
ic.update(y,x,z)
noi.update(y,x,z)

x = -3
z = 5
y = 2*x +3*z - 5
ic.update(y,x,z)
noi.update(y,x,z)

x = 0
z = 0
y = 2*x +3*z - 5
ic.update(y,x,z)
noi.update(y,x,z)


#X.update(13.0-12,12.0-13)
#X.update(14.0-12,11.0-13)
print ("C11=",ic.vx()) 
print ("ybar",ic.ybar() )
print ("xbar",ic.xbar() )
print ("zbar",ic.zbar() )
print ("alpha=",ic.alpha())
print ("b1=",ic.betax())
print ("b2=",ic.betaz())
print()
print ("C11=",noi.vx()) 
print ("ybar",noi.ybar() )
print ("xbar",noi.xbar() )
print ("zbar",noi.zbar() )
print ("alpha=",noi.alpha())
print ("b1=",noi.betax())
print ("b2=",noi.betaz())
